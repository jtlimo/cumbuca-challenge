package domain

import "github.com/google/uuid"

type AccountRepository interface {
	CreateAccount(account *Account) error
	GetById(uuid uuid.UUID) (*Account, error)
	GetBalance(uuid string) (int, error)
	UpdateBalance(account *Account) error
}
