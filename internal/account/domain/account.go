package domain

import (
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/paemuri/brdoc"
)

type Account struct {
	AccountDTO
	Id        uint
	AccountId uuid.UUID `db:"account_id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type AccountDTO struct {
	Name     string `db:"name"`
	LastName string `db:"last_name"`
	CPF      string `db:"cpf"`
	Balance  int    `db:"balance"`
}

func New(acc AccountDTO) (*Account, error) {
	balance := acc.Balance
	cpf := acc.CPF

	sanitizedName := strings.TrimSpace(acc.Name)
	sanitizedLastName := strings.TrimSpace(acc.LastName)

	if strings.TrimSpace(sanitizedName) == "" || strings.TrimSpace(sanitizedLastName) == "" {
		return nil, errors.New("invalid name or last name")
	}

	if !brdoc.IsCPF(cpf) {
		return nil, errors.New("invalid cpf")
	}

	if balance < 0 {
		return nil, errors.New("invalid balance")
	}

	account := &Account{
		AccountId: uuid.New(),
		AccountDTO: AccountDTO{
			Name:     sanitizedName,
			LastName: sanitizedLastName,
			CPF:      cpf,
			Balance:  balance,
		},
		CreatedAt: time.Now(),
	}

	return account, nil
}
