package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	t.Run("create a standard account successfully", func(t *testing.T) {
		expectedAccount := &Account{
			AccountDTO: AccountDTO{
				Name:     "Jessica",
				LastName: "Lima",
				CPF:      "57900366008",
				Balance:  100,
			},
		}

		accountDTO := AccountDTO{Name: "Jessica", LastName: "Lima", CPF: "57900366008", Balance: 100}
		account, _ := New(accountDTO)

		assert.Equal(t, expectedAccount.AccountDTO, account.AccountDTO)
	})

	t.Run("throws an error when try to create an account with negative balance", func(t *testing.T) {
		accountDTO := AccountDTO{Name: "Jessica", LastName: "Lima", CPF: "85001563097", Balance: -100}
		_, err := New(accountDTO)

		assert.ErrorContains(t, err, "invalid balance")
	})

	t.Run("create an account successfully when the balance is 0", func(t *testing.T) {
		expectedAccount := &Account{
			AccountDTO: AccountDTO{
				Name:     "Jessica",
				LastName: "Lima",
				CPF:      "71891990047",
				Balance:  0,
			},
		}

		accountDTO := AccountDTO{Name: "Jessica", LastName: "     Lima     ", CPF: "71891990047", Balance: 0}
		account, _ := New(accountDTO)

		assert.Equal(t, expectedAccount.AccountDTO, account.AccountDTO)
	})

	t.Run("throws an error when try to create an account with an invalid cpf", func(t *testing.T) {
		accountDTO := AccountDTO{Name: "Jessica", LastName: "Lima", CPF: "00000000000", Balance: 10000}
		_, err := New(accountDTO)

		assert.ErrorContains(t, err, "invalid cpf")
	})

	t.Run("throws an error when try to create an account with empty name", func(t *testing.T) {
		accountDTO := AccountDTO{Name: "", LastName: "Lima", CPF: "71891990047", Balance: 1000}

		_, err := New(accountDTO)

		assert.ErrorContains(t, err, "invalid name or last name")
	})

	t.Run("throws an error when try to create an account with empty lastName", func(t *testing.T) {
		accountDTO := AccountDTO{Name: "Jessica", LastName: "", CPF: "71891990047", Balance: 10000}

		_, err := New(accountDTO)

		assert.ErrorContains(t, err, "invalid name or last name")
	})

	t.Run("throws lastName empty error instead of invalid cpf", func(t *testing.T) {
		accountDTO := AccountDTO{Name: "Jessica", LastName: "", CPF: "00000", Balance: 10000}

		_, err := New(accountDTO)

		assert.ErrorContains(t, err, "invalid name or last name")
	})

	t.Run("throws an error when try to create an account with whitespaced lastName", func(t *testing.T) {
		accountDTO := AccountDTO{Name: "Jessica", LastName: "         ", CPF: "71891990047", Balance: 10000}

		_, err := New(accountDTO)

		assert.ErrorContains(t, err, "invalid name or last name")
	})
}
