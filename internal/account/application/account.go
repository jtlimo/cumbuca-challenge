package application

import (
	"cumbuca/internal/account/domain"
	"cumbuca/internal/account/repository"
)

type AccountUC struct {
	repository *repository.AccountRepository
}
type AccountUseCase interface {
	NewAccountUC(repository *repository.AccountRepository) *AccountUC
	Create(account *domain.Account) error
	GetBalance(accountId string) error
}
