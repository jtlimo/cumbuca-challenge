package application

import (
	"cumbuca/internal/account/domain"
	"cumbuca/internal/account/repository"
)

func NewAccountUC(repository *repository.AccountRepository) *AccountUC {
	return &AccountUC{
		repository: repository,
	}
}

func (a *AccountUC) Create(account *domain.Account) error {
	err := a.repository.CreateAccount(account)
	if err != nil {
		return err
	}
	return nil
}
