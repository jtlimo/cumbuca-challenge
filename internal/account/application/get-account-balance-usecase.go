package application

func (a *AccountUC) GetBalance(accountId string) (int, error) {
	balance, err := a.repository.GetBalance(accountId)
	if err != nil {
		return 0, err
	}
	return balance, nil
}
