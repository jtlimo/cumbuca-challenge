package application

import (
	account "cumbuca/internal/account/domain"
	"cumbuca/internal/account/repository"
	database "cumbuca/tests"
	"os"
	"testing"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var testDB *sqlx.DB

func TestMain(m *testing.M) {
	testDB = database.InitTempDB()
	defer testDB.Close()
	os.Exit(m.Run())
}

func TestCreate(t *testing.T) {
	accountRepository, _ := repository.New(testDB)
	useCase := NewAccountUC(accountRepository)
	accountDTO := account.AccountDTO{Name: "Rosalía", LastName: "Tobella", CPF: "82327535018", Balance: 1000}
	acc, _ := account.New(accountDTO)

	err := useCase.Create(acc)

	assert.NoError(t, err)
}
