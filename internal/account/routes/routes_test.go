package routes

import (
	"bytes"
	"cumbuca/internal/account/application"
	"cumbuca/internal/account/domain"
	"cumbuca/internal/account/repository"
	"cumbuca/internal/account/routes/adapters"
	"cumbuca/pkg/routes"
	setup "cumbuca/tests"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var testDB *sqlx.DB
var repo *repository.AccountRepository
var s Server
var expectedAccount domain.Account

func TestMain(m *testing.M) {
	testDB = setup.InitTempDB()
	defer testDB.Close()
	repo, _ = repository.New(testDB)

	s = Server{
		Router:         mux.NewRouter(),
		AccountUseCase: application.NewAccountUC(repo),
		Middleware:     routes.New(repo),
	}

	uuid.SetRand(rand.New(rand.NewSource(1)))
	uuid := setup.AccountUUID

	expectedAccount = domain.Account{
		AccountId: uuid,
		AccountDTO: domain.AccountDTO{
			Name:     "Maria",
			LastName: "do Bairro",
			CPF:      "06395184008",
			Balance:  100,
		},
	}

	repo.CreateAccount(&expectedAccount)

	code := m.Run()
	os.Exit(code)
}

func TestCreateAccount(t *testing.T) {
	newServer(t)
	t.Run("create an account", func(t *testing.T) {
		account := adapters.AccountResponse{
			Id:        setup.AccountUUID,
			Name:      "Maria",
			LastName:  "do Bairro",
			CPF:       "06395184008",
			Balance:   100,
			CreatedAt: setup.Now,
		}

		payload := adapters.AccountRequestBody{
			Name:     "Maria",
			LastName: "do Bairro",
			CPF:      "06395184008",
			Balance:  100,
		}

		request := createAccount(payload)
		res := executeRequest(request)

		body, _ := io.ReadAll(res.Body)

		var jsonData adapters.AccountResponse
		json.Unmarshal(body, &jsonData)

		if assert.NotNil(t, jsonData) {
			assert.Equal(t, account.Name, jsonData.Name)
			assert.Equal(t, account.LastName, jsonData.LastName)
			assert.Equal(t, account.CPF, jsonData.CPF)
			assert.Equal(t, account.Balance, jsonData.Balance)
		}
		assertStatus(t, res.Code, http.StatusOK)
	})
}

func TestGetAccountBalance(t *testing.T) {
	newServer(t)
	t.Run("get a balance from an account", func(t *testing.T) {
		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := setup.AccountUUID

		account := adapters.AccountResponse{
			Id:        uuid,
			Name:      "Maria",
			LastName:  "do Bairro",
			CPF:       "06395184008",
			Balance:   100,
			CreatedAt: setup.Now,
		}

		authRequest := authenticate(adapters.AuthenticateRequestBody{AccountId: account.Id.String()})
		authResponse := executeRequest(authRequest)

		authBody, _ := io.ReadAll(authResponse.Body)

		var jsonAuthData adapters.AuthenticateResponse
		json.Unmarshal(authBody, &jsonAuthData)

		rr := getAccountBalance(adapters.GetAccountBalanceRequestBody{AccountId: account.Id.String()}, jsonAuthData.Token)
		res := executeRequest(rr)

		body, _ := io.ReadAll(res.Body)

		var jsonData adapters.GetAccountBalanceResponse
		json.Unmarshal(body, &jsonData)

		if assert.NotNil(t, jsonData) {
			assert.Equal(t, account.Balance, jsonData.Balance)
		}
		assertStatus(t, res.Code, http.StatusOK)
	})

	t.Run("got unauthorized when try to get balance with an inexistent user", func(t *testing.T) {
		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := setup.AccountUUID
		expectedUUID := setup.AuthUUID

		expectedAccount = domain.Account{
			AccountId: expectedUUID,
			AccountDTO: domain.AccountDTO{
				Name:     "Josélia",
				LastName: "Terra",
				CPF:      "06395184008",
				Balance:  1000000,
			},
			CreatedAt: setup.Now,
		}

		repo.CreateAccount(&expectedAccount)

		account := adapters.AuthenticateRequestBody{
			AccountId: uuid.String(),
		}

		authRequest := authenticate(adapters.AuthenticateRequestBody{AccountId: account.AccountId})
		authResponse := executeRequest(authRequest)

		authBody, _ := io.ReadAll(authResponse.Body)

		var jsonAuthData adapters.AuthenticateResponse
		json.Unmarshal(authBody, &jsonAuthData)

		rr := getAccountBalance(adapters.GetAccountBalanceRequestBody{AccountId: expectedAccount.AccountId.String()}, jsonAuthData.Token)
		res := executeRequest(rr)

		assertStatus(t, res.Code, http.StatusUnauthorized)
	})

	t.Run("got unauthorized when try to get balance with an user that not expected to do that action", func(t *testing.T) {
		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := setup.AccountUUID
		expectedUUID := setup.AuthUUID

		expectedAccount = domain.Account{
			AccountId: expectedUUID,
			AccountDTO: domain.AccountDTO{
				Name:     "Josélia",
				LastName: "Terra",
				CPF:      "06395184008",
				Balance:  1000000,
			},
			CreatedAt: setup.Now,
		}

		repo.CreateAccount(&expectedAccount)

		acc := domain.Account{
			AccountId: uuid,
			AccountDTO: domain.AccountDTO{
				Name:     "Dáphine",
				LastName: "Marcelle",
				CPF:      "06395184008",
				Balance:  20000000000000,
			},
			CreatedAt: setup.Now,
		}

		repo.CreateAccount(&acc)

		account := adapters.AuthenticateRequestBody{
			AccountId: acc.AccountId.String(),
		}

		authRequest := authenticate(adapters.AuthenticateRequestBody{AccountId: account.AccountId})
		authResponse := executeRequest(authRequest)

		authBody, _ := io.ReadAll(authResponse.Body)

		var jsonAuthData adapters.AuthenticateResponse
		json.Unmarshal(authBody, &jsonAuthData)

		rr := getAccountBalance(adapters.GetAccountBalanceRequestBody{AccountId: expectedAccount.AccountId.String()}, jsonAuthData.Token)
		res := executeRequest(rr)

		assertStatus(t, res.Code, http.StatusUnauthorized)
	})

	t.Run("got unauthorized when try to get balance without authentication", func(t *testing.T) {
		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := setup.AccountUUID

		account := adapters.AccountResponse{
			Id:        uuid,
			Name:      "Maria",
			LastName:  "do Bairro",
			CPF:       "06395184008",
			Balance:   100,
			CreatedAt: time.Now(),
		}

		rr := getAccountBalance(adapters.GetAccountBalanceRequestBody{AccountId: account.Id.String()}, "")
		res := executeRequest(rr)

		assertStatus(t, res.Code, http.StatusUnauthorized)
	})
}

func TestAuthentication(t *testing.T) {
	newServer(t)
	t.Run("returns a valid token", func(t *testing.T) {
		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := setup.AccountUUID

		account := adapters.AccountResponse{
			Id:        uuid,
			Name:      "Maria",
			LastName:  "do Bairro",
			CPF:       "06395184008",
			Balance:   100,
			CreatedAt: time.Now(),
		}

		rr := authenticate(adapters.AuthenticateRequestBody{AccountId: account.Id.String()})
		res := executeRequest(rr)

		body, _ := io.ReadAll(res.Body)

		var jsonData adapters.AuthenticateResponse
		json.Unmarshal(body, &jsonData)

		assert.NotNil(t, jsonData.Token)
		assertStatus(t, res.Code, http.StatusOK)
	})
}

func newServer(t *testing.T) {
	t.Helper()
	s.RegisterWithAuthMW()
	s.RegisterWithoutAuthMW()
	localServer := httptest.NewServer(s.Router)
	defer localServer.Close()
}

func createAccount(payload adapters.AccountRequestBody) *http.Request {
	body, _ := json.Marshal(payload)
	req, _ := http.NewRequest(http.MethodPost, "/account", bytes.NewBuffer(body))

	return req
}

func getAccountBalance(payload adapters.GetAccountBalanceRequestBody, jwtToken string) *http.Request {
	accountId := payload.AccountId
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/account/%s/balance", accountId), nil)

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", jwtToken))

	return req
}

func authenticate(payload adapters.AuthenticateRequestBody) *http.Request {
	body, _ := json.Marshal(payload)
	req, _ := http.NewRequest(http.MethodPost, "/auth", bytes.NewBuffer(body))

	return req
}

func assertStatus(t *testing.T, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("did not get correct status, got %d, want %d", got, want)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	s.Router.ServeHTTP(rr, req)

	return rr
}
