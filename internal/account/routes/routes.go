package routes

import (
	"cumbuca/internal/account/application"
	"cumbuca/internal/account/routes/adapters"
	"cumbuca/pkg/config"
	"cumbuca/pkg/routes"
	"encoding/json"
	"io"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	Router         *mux.Router
	AccountUseCase *application.AccountUC
	Config         *config.Config
	Middleware     *routes.Middleware
}

func (s *Server) RegisterWithoutAuthMW() {
	s.Router.HandleFunc("/account", s.createAccount).Methods(http.MethodPost)
	s.Router.HandleFunc("/auth", s.Authenticate).Methods(http.MethodPost)
}

func (s *Server) RegisterWithAuthMW() {
	protected := s.Router.PathPrefix("/account").Subrouter()
	protected.Use(s.Middleware.Authentication)
	protected.HandleFunc("/{account_id}/balance", s.getAccountBalance).Methods(http.MethodGet)
}

func (s *Server) Authenticate(w http.ResponseWriter, r *http.Request) {
	body, _ := io.ReadAll(r.Body)

	var jsonData adapters.AuthenticateRequestBody
	json.Unmarshal(body, &jsonData)

	if len(jsonData.AccountId) == 0 {
		http.Error(w, "Please provide your accountId to obtain the token", http.StatusBadRequest)
		return
	}
	token, err := routes.CreateToken(jsonData.AccountId)
	if err != nil {
		http.Error(w, "Error when trying to create a token", http.StatusBadRequest)
		return
	}

	authToken, err := json.Marshal(adapters.AdaptAuthToResponse(token))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	w.Write(authToken)
}

func (s *Server) createAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var accountRequest adapters.AccountRequestBody

	if err := json.NewDecoder(r.Body).Decode(&accountRequest); err != nil {
		http.Error(w, "Error decoding response object", http.StatusBadRequest)
		return
	}

	_, err := json.Marshal(&accountRequest)
	if err != nil {
		http.Error(w, "Error encoding response object", http.StatusInternalServerError)
		return
	}

	account, err := adapters.AdaptToDomain(accountRequest)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	accountResponse, err := json.Marshal(adapters.AdaptToResponse(account))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	err = s.AccountUseCase.Create(account)
	if err != nil {
		http.Error(w, "Error when trying to create an account", http.StatusBadRequest)
		return
	}

	w.Write(accountResponse)
}

func (s *Server) getAccountBalance(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	accountId := vars["account_id"]
	accountIdFromHeader := r.Header.Get("AccountId")

	w.Header().Set("Content-Type", "application/json")

	if accountIdFromHeader != accountId {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	balance, err := s.AccountUseCase.GetBalance(accountId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	balanceResponse, err := json.Marshal(adapters.AdaptBalanceToResponse(balance))

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	w.Write(balanceResponse)
}
