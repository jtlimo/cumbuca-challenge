package adapters

type GetAccountBalanceRequestBody struct {
	AccountId string `json:"account_id"`
}

type GetAccountBalanceResponse struct {
	Balance int `json:"balance"`
}
