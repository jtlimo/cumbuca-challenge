package adapters

type AuthenticateRequestBody struct {
	AccountId string `json:"accountId"`
}

type AuthenticateResponse struct {
	Token string `json:"token"`
}
