package adapters

import (
	account "cumbuca/internal/account/domain"
)

func AdaptToDomain(accountRequestBody AccountRequestBody) (*account.Account, error) {
	return account.New(account.AccountDTO(accountRequestBody))
}

func AdaptToResponse(account *account.Account) AccountResponse {
	return AccountResponse{
		Id:        account.AccountId,
		Name:      account.Name,
		LastName:  account.LastName,
		CPF:       account.CPF,
		Balance:   account.Balance,
		CreatedAt: account.CreatedAt,
	}
}

func AdaptAuthToResponse(token string) AuthenticateResponse {
	return AuthenticateResponse{
		Token: token,
	}
}

func AdaptBalanceToResponse(balance int) GetAccountBalanceResponse {
	return GetAccountBalanceResponse{
		Balance: balance,
	}
}
