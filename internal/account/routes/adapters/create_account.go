package adapters

import (
	"time"

	"github.com/google/uuid"
)

type AccountRequestBody struct {
	Name     string `json:"name"`
	LastName string `json:"lastName"`
	CPF      string `json:"cpf"`
	Balance  int    `json:"balance"`
}

type AccountResponse struct {
	Id        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	LastName  string    `json:"lastName"`
	CPF       string    `json:"cpf"`
	Balance   int       `json:"balance"`
	CreatedAt time.Time `json:"createdAt"`
}
