package repository

import (
	account "cumbuca/internal/account/domain"
	database "cumbuca/tests"
	"math/rand"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCreateAccount(t *testing.T) {
	testDB := database.InitTempDB()
	defer testDB.Close()
	t.Run("create an account succesfully", func(t *testing.T) {
		repo, _ := New(testDB)

		accountDTO := account.AccountDTO{Name: "Perrie", LastName: "Edwards", CPF: "08505165012", Balance: 1000}
		account, _ := account.New(accountDTO)

		err := repo.CreateAccount(account)

		assert.NoError(t, err)

	})
}

func TestGetById(t *testing.T) {
	testDB := database.InitTempDB()
	defer testDB.Close()
	t.Run("returns a specific account", func(t *testing.T) {
		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := uuid.MustParse("52fdfc07-2182-454f-963f-5f0f9a621d72")
		repo, _ := New(testDB)

		expectedAccount := account.Account{
			AccountId: uuid,
			AccountDTO: account.AccountDTO{
				Name:     "Perrie",
				LastName: "Edwards",
				CPF:      "08505165012",
				Balance:  1000},
		}

		repo.CreateAccount(&expectedAccount)

		acc, err := repo.GetById(uuid)

		assert.NoError(t, err)
		if assert.NotEmpty(t, acc) || assert.NotNil(t, acc) {
			assert.Equal(t, expectedAccount.AccountDTO, acc.AccountDTO)
		}
	})

	t.Run("returns an error when account not found", func(t *testing.T) {
		repo, _ := New(testDB)

		uuid.SetRand(rand.New(rand.NewSource(1)))
		uuid := uuid.MustParse("52fdfc07-2182-454f-963f-5f0f9a621d73")

		_, err := repo.GetById(uuid)

		assert.ErrorContains(t, err, "account not found")
	})
}
