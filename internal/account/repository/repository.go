package repository

import (
	"cumbuca/internal/account/domain"
	"errors"
	"sync"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type AccountRepository struct {
	db *sqlx.DB
	mu sync.Mutex
}

func New(db *sqlx.DB) (*AccountRepository, error) {
	return &AccountRepository{
		db: db,
	}, nil
}

func (ar *AccountRepository) CreateAccount(account *domain.Account) error {
	ar.mu.Lock()
	stmt, err := ar.db.Preparex(`INSERT INTO accounts (account_id, name, last_name, cpf, balance, created_at)
	 	 VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`)

	if err != nil {
		ar.mu.Unlock()
		return err
	}

	if _, err := stmt.Exec(
		account.AccountId,
		account.Name,
		account.LastName,
		account.CPF,
		account.Balance,
		account.CreatedAt,
	); err != nil {
		return errors.New("error creating account")
	}
	ar.mu.Unlock()

	return nil
}

func (ar *AccountRepository) GetById(uuid uuid.UUID) (*domain.Account, error) {
	stmt, err := ar.db.Preparex("SELECT * FROM accounts WHERE account_id = $1")

	if err != nil {
		return &domain.Account{}, errors.New("error when preparing statement")
	}

	var account domain.Account

	if err := stmt.Get(&account, uuid); err != nil {
		return &domain.Account{}, errors.New("account not found")
	}
	return &account, nil
}

func (ar *AccountRepository) GetBalance(uuid string) (int, error) {
	stmt, err := ar.db.Preparex("SELECT balance FROM accounts WHERE account_id = $1")

	if err != nil {
		return 0, errors.New("error when preparing statement")
	}

	var account domain.Account

	if err := stmt.Get(&account, uuid); err != nil {
		return 0, errors.New("account not found")
	}
	return account.AccountDTO.Balance, nil
}

func (ar *AccountRepository) UpdateBalance(account *domain.Account) error {
	ar.mu.Lock()
	stmt, err := ar.db.Preparex("UPDATE accounts SET balance = $1, updated_at = $2 WHERE account_id = $3")

	if err != nil {
		ar.mu.Unlock()
		return err
	}

	if _, err := stmt.Exec(
		account.Balance,
		account.UpdatedAt,
		account.AccountId,
	); err != nil {
		return errors.New("error when updating account balance")
	}
	ar.mu.Unlock()

	return nil
}
