package domain

import (
	"errors"
	"time"

	"github.com/google/uuid"
)

type Transaction struct {
	TransactionDTO
	Id            uint
	TransactionId uuid.UUID `db:"transaction_id"`
	CreatedAt     time.Time `db:"created_at"`
}

type TransactionDTO struct {
	AccountSender   uuid.UUID `db:"account_sender"`
	AccountReceiver uuid.UUID `db:"account_receiver"`
	Value           int       `db:"value"`
}

func New(t TransactionDTO) (*Transaction, error) {
	value := t.Value
	accountReceiver := t.AccountReceiver
	accountSender := t.AccountSender

	if value <= 0 {
		return nil, errors.New("cannot create a transaction with negative value")
	}

	if accountReceiver == uuid.Nil || accountSender == uuid.Nil {
		return nil, errors.New("cannot create a transaction without account receiver or sender")
	}

	transaction := &Transaction{
		TransactionId: uuid.New(),
		TransactionDTO: TransactionDTO{
			AccountReceiver: accountReceiver,
			AccountSender:   accountSender,
			Value:           value,
		},
		CreatedAt: time.Now(),
	}

	return transaction, nil
}
