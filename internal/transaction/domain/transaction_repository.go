package domain

import "github.com/google/uuid"

type TransactionRepository interface {
	CreateTransaction(transaction *Transaction) error
	GetById(uuid uuid.UUID) (*Transaction, error)
}
