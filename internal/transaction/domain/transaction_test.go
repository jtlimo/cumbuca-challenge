package domain

import (
	setup "cumbuca/tests"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	t.Run("create a transaction successfully", func(t *testing.T) {
		expectedTransaction := &Transaction{
			TransactionDTO: TransactionDTO{
				AccountReceiver: setup.AccountReceiverUUID,
				AccountSender:   setup.AccountSenderUUID,
				Value:           100,
			},
		}

		transactionDTO := TransactionDTO{AccountReceiver: setup.AccountReceiverUUID, AccountSender: setup.AccountSenderUUID, Value: 100}
		transaction, _ := New(transactionDTO)

		assert.Equal(t, expectedTransaction.TransactionDTO, transaction.TransactionDTO)
	})

	t.Run("throws an error when try to create a transaction with negative balance", func(t *testing.T) {
		transactionDTO := TransactionDTO{AccountReceiver: setup.AccountReceiverUUID, AccountSender: setup.AccountSenderUUID, Value: -100}
		_, err := New(transactionDTO)

		assert.ErrorContains(t, err, "cannot create a transaction with negative value")
	})

	t.Run("throws an error when try to create a transaction without receiver", func(t *testing.T) {
		transactionDTO := TransactionDTO{AccountSender: setup.AccountSenderUUID, Value: 10000}
		_, err := New(transactionDTO)

		assert.ErrorContains(t, err, "cannot create a transaction without account receiver or sender")
	})

	t.Run("throws an error when try to create a transaction without sender", func(t *testing.T) {
		transactionDTO := TransactionDTO{AccountReceiver: setup.AccountReceiverUUID, Value: 10000}
		_, err := New(transactionDTO)

		assert.ErrorContains(t, err, "cannot create a transaction without account receiver or sender")
	})

	t.Run("throws an error when try to create a transaction without sender and receiver", func(t *testing.T) {
		transactionDTO := TransactionDTO{Value: 10000}
		_, err := New(transactionDTO)

		assert.ErrorContains(t, err, "cannot create a transaction without account receiver or sender")
	})
}
