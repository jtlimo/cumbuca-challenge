package application

import (
	account "cumbuca/internal/account/domain"
	ar "cumbuca/internal/account/repository"
	"cumbuca/internal/transaction/domain"
	"cumbuca/internal/transaction/repository"
	"errors"
	"time"
)

func NewTransactionUC(accountRepository *ar.AccountRepository, repository *repository.TransactionRepository) *TransactionUC {
	return &TransactionUC{
		repository:        repository,
		accountRepository: accountRepository,
	}
}

func (t *TransactionUC) Create(transaction *domain.Transaction) error {
	accountSenderUUID := transaction.AccountSender
	accountReceiverUUID := transaction.AccountReceiver
	transactionValue := transaction.Value

	accountReceiver, err := t.accountRepository.GetById(accountReceiverUUID)
	if err != nil {
		return errors.New("cannot make a transaction to an inexistent account")
	}

	accountSender, err := t.accountRepository.GetById(accountSenderUUID)
	if err != nil && err.Error() == "account not found" {
		return errors.New("cannot make a transaction from an inexistent account")
	}

	if accountSender.Balance < transactionValue {
		return errors.New("cannot make a transaction with insufficient balance")
	}

	if err = t.repository.CreateTransaction(transaction); err != nil {
		return err
	}

	if err = t.makeP2PTransfer(accountSender, accountReceiver, transactionValue); err != nil {
		return err
	}

	return nil
}

func (t *TransactionUC) makeP2PTransfer(accountSender *account.Account, accountReceiver *account.Account, value int) error {
	accountSender.Balance -= value
	accountReceiver.Balance += value
	accountSender.UpdatedAt = time.Now()
	accountReceiver.UpdatedAt = time.Now()

	if err := t.accountRepository.UpdateBalance(accountSender); err != nil {
		return err
	}

	if err := t.accountRepository.UpdateBalance(accountReceiver); err != nil {
		return err
	}

	return nil
}
