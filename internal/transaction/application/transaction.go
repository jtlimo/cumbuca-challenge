package application

import (
	account "cumbuca/internal/account/repository"
	"cumbuca/internal/transaction/domain"
	"cumbuca/internal/transaction/repository"
)

type TransactionUC struct {
	accountRepository *account.AccountRepository
	repository        *repository.TransactionRepository
}

type TransactionUseCase interface {
	NewTransactionUC(repository *repository.TransactionRepository, accountRepository *account.AccountRepository) *TransactionUC
	Create(transaction *domain.Transaction) error
}
