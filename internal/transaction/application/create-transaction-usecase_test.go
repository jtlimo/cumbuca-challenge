package application

import (
	account "cumbuca/internal/account/domain"
	account_repository "cumbuca/internal/account/repository"
	transaction "cumbuca/internal/transaction/domain"
	"cumbuca/internal/transaction/repository"
	setup "cumbuca/tests"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var testDB *sqlx.DB

func TestMain(m *testing.M) {
	testDB = setup.InitTempDB()
	defer testDB.Close()
	os.Exit(m.Run())
}

func TestCreate(t *testing.T) {
	t.Run("throws an error when try to make a transaction with insufficient balance", func(t *testing.T) {
		accountRepository, _ := account_repository.New(testDB)
		repository, _ := repository.New(testDB)
		insufficientBalanceUUID := uuid.MustParse("1aae85ce-8be1-4d83-a29c-0ab29d4f9c7c")
		receiverUUID := uuid.MustParse("11ce0d82-41e6-4634-8c85-f215260bcd4e")

		accountDTO := account.AccountDTO{Name: "Bráulio", LastName: "Marcelle", CPF: "87416135036", Balance: 100}

		accountWithInsufficientBalance := &account.Account{
			AccountId:  insufficientBalanceUUID,
			AccountDTO: accountDTO,
			CreatedAt:  setup.Now,
		}

		accountRepository.CreateAccount(accountWithInsufficientBalance)

		accountReceiverDTO := account.AccountDTO{Name: "Ricardo", LastName: "Marcelle", CPF: "87416135036", Balance: 100}

		accountReceiver := &account.Account{
			AccountId:  receiverUUID,
			AccountDTO: accountReceiverDTO,
			CreatedAt:  setup.Now,
		}

		accountRepository.CreateAccount(accountReceiver)

		useCase := NewTransactionUC(accountRepository, repository)

		transactionDTO := transaction.TransactionDTO{AccountReceiver: receiverUUID, AccountSender: insufficientBalanceUUID, Value: 200}
		transaction, _ := transaction.New(transactionDTO)

		err := useCase.Create(transaction)

		assert.ErrorContains(t, err, "cannot make a transaction with insufficient balance")
	})

	t.Run("throws an error when try to make a transaction to an inexistent account", func(t *testing.T) {
		accountRepository, _ := account_repository.New(testDB)
		repository, _ := repository.New(testDB)
		senderUUID := uuid.MustParse("58131002-006a-4fc4-95ce-14b6c16eaed5")
		accountDTO := account.AccountDTO{Name: "Bráulio", LastName: "Marcelle", CPF: "87416135036", Balance: 120}

		account := &account.Account{
			AccountId:  senderUUID,
			AccountDTO: accountDTO,
			CreatedAt:  setup.Now,
		}

		accountRepository.CreateAccount(account)

		useCase := NewTransactionUC(accountRepository, repository)

		transactionDTO := transaction.TransactionDTO{AccountReceiver: setup.AccountReceiverInexistentUUID, AccountSender: senderUUID, Value: 100}
		transaction, _ := transaction.New(transactionDTO)

		err := useCase.Create(transaction)

		assert.ErrorContains(t, err, "cannot make a transaction to an inexistent account")
	})

	t.Run("throws an error when try to make a transaction from an inexistent account", func(t *testing.T) {
		accountRepository, _ := account_repository.New(testDB)
		repository, _ := repository.New(testDB)
		inexistentUUID := uuid.MustParse("3a0d6749-0ad1-4796-b24b-40928b9b256f")
		receiverUUID := uuid.MustParse("a89a5396-d8b8-460c-a78e-4d2a354d69d6")
		accountDTO := account.AccountDTO{Name: "Bráulio", LastName: "Marcelle", CPF: "87416135036", Balance: 120}

		account := &account.Account{
			AccountId:  receiverUUID,
			AccountDTO: accountDTO,
			CreatedAt:  setup.Now,
		}

		accountRepository.CreateAccount(account)

		useCase := NewTransactionUC(accountRepository, repository)

		transactionDTO := transaction.TransactionDTO{AccountReceiver: receiverUUID, AccountSender: inexistentUUID, Value: 100}
		transaction, _ := transaction.New(transactionDTO)

		err := useCase.Create(transaction)

		assert.ErrorContains(t, err, "cannot make a transaction from an inexistent account")
	})

	t.Run("debit balance from sender account and credit value to receiver account after make a transaction succesfully", func(t *testing.T) {
		accountRepository, _ := account_repository.New(testDB)
		repository, _ := repository.New(testDB)

		expectedSenderBalance := 1100
		expectedReceiverBalance := 100

		accountSenderDTO := account.AccountDTO{Name: "Bráulio", LastName: "Marcelle", CPF: "87416135036", Balance: 1200}
		accountReceiverDTO := account.AccountDTO{Name: "James", LastName: "Rodriguez", CPF: "28840655042", Balance: 0}

		accountSender := &account.Account{
			AccountId:  setup.AccountSenderUUID,
			AccountDTO: accountSenderDTO,
			CreatedAt:  setup.Now,
		}

		accountReceiver := &account.Account{
			AccountId:  setup.AccountReceiverUUID,
			AccountDTO: accountReceiverDTO,
			CreatedAt:  setup.Now,
		}

		accountRepository.CreateAccount(accountSender)
		accountRepository.CreateAccount(accountReceiver)

		useCase := NewTransactionUC(accountRepository, repository)

		transactionDTO := transaction.TransactionDTO{AccountReceiver: setup.AccountReceiverUUID, AccountSender: setup.AccountSenderUUID, Value: 100}
		transaction, _ := transaction.New(transactionDTO)

		err := useCase.Create(transaction)
		accountSenderBalance, _ := accountRepository.GetBalance(setup.AccountSenderUUID.String())
		accountReceiverBalance, _ := accountRepository.GetBalance(setup.AccountReceiverUUID.String())

		assert.NoError(t, err)
		assert.Equal(t, expectedSenderBalance, accountSenderBalance)
		assert.Equal(t, expectedReceiverBalance, accountReceiverBalance)
	})
}
