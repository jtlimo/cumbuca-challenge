package repository

import (
	"cumbuca/internal/transaction/domain"
	"errors"
	"sync"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type TransactionRepository struct {
	db *sqlx.DB
	mu sync.Mutex
}

func New(db *sqlx.DB) (*TransactionRepository, error) {
	return &TransactionRepository{
		db: db,
	}, nil
}

func (tr *TransactionRepository) CreateTransaction(transaction *domain.Transaction) error {
	tr.mu.Lock()
	stmt, err := tr.db.Preparex(`INSERT INTO transactions (transaction_id, account_receiver, account_sender, value, created_at)
	 	 VALUES ($1, $2, $3, $4, $5) RETURNING *`)

	if err != nil {
		tr.mu.Unlock()
		return err
	}

	if _, err := stmt.Exec(
		transaction.TransactionId,
		transaction.AccountReceiver,
		transaction.AccountSender,
		transaction.Value,
		transaction.CreatedAt,
	); err != nil {
		return errors.New("error when creating transaction")
	}
	tr.mu.Unlock()

	return nil
}

func (tr *TransactionRepository) GetById(uuid uuid.UUID) (*domain.Transaction, error) {
	stmt, err := tr.db.Preparex("SELECT * FROM transactions WHERE transaction_id = $1")

	if err != nil {
		return &domain.Transaction{}, errors.New("error when preparing statement")
	}

	var transaction domain.Transaction

	if err := stmt.Get(&transaction, uuid); err != nil {
		return &domain.Transaction{}, errors.New("transaction not found")
	}
	return &transaction, nil
}
