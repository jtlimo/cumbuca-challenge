package repository

import (
	transaction "cumbuca/internal/transaction/domain"
	setup "cumbuca/tests"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateTransaction(t *testing.T) {
	testDB := setup.InitTempDB()
	defer testDB.Close()
	t.Run("create a transaction succesfully", func(t *testing.T) {
		repo, _ := New(testDB)

		transactionDTO := transaction.TransactionDTO{AccountReceiver: setup.AccountReceiverUUID, AccountSender: setup.AccountSenderUUID, Value: 1000}
		transaction, _ := transaction.New(transactionDTO)

		err := repo.CreateTransaction(transaction)

		assert.NoError(t, err)
	})
}

func TestGetById(t *testing.T) {
	testDB := setup.InitTempDB()
	defer testDB.Close()
	t.Run("returns a specific transaction", func(t *testing.T) {
		repo, _ := New(testDB)

		expectedTransaction := transaction.Transaction{
			TransactionId: setup.TransactionUUID,
			TransactionDTO: transaction.TransactionDTO{
				AccountReceiver: setup.AccountReceiverUUID,
				AccountSender:   setup.AccountSenderUUID,
				Value:           1000,
			},
			CreatedAt: setup.Now,
		}

		repo.CreateTransaction(&expectedTransaction)

		transaction, err := repo.GetById(setup.TransactionUUID)

		assert.NoError(t, err)
		if assert.NotEmpty(t, transaction) || assert.NotNil(t, transaction) {
			assert.Equal(t, expectedTransaction.TransactionId, transaction.TransactionId)
			assert.Equal(t, expectedTransaction.TransactionDTO, transaction.TransactionDTO)
		}
	})

	t.Run("returns an error when transaction not found", func(t *testing.T) {
		repo, _ := New(testDB)

		_, err := repo.GetById(setup.TransactionNotFoundUUID)

		assert.ErrorContains(t, err, "transaction not found")
	})
}
