package routes

import (
	"cumbuca/internal/transaction/application"
	"cumbuca/internal/transaction/routes/adapters"
	"cumbuca/pkg/config"
	"cumbuca/pkg/routes"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type Server struct {
	Router             *mux.Router
	TransactionUseCase *application.TransactionUC
	Config             *config.Config
	Middleware         *routes.Middleware
}

func (s *Server) RegisterWithAuthMW() {
	protected := s.Router.PathPrefix("/transaction").Subrouter()
	protected.Use(s.Middleware.Authentication)
	protected.HandleFunc("", s.createTransaction).Methods(http.MethodPost)
}

func (s *Server) createTransaction(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var transactionRequest adapters.TransactionRequestBody

	if err := json.NewDecoder(r.Body).Decode(&transactionRequest); err != nil {
		http.Error(w, "Error decoding response object", http.StatusBadRequest)
		return
	}

	_, err := json.Marshal(&transactionRequest)
	if err != nil {
		http.Error(w, "Error encoding response object", http.StatusInternalServerError)
		return
	}

	transaction, err := adapters.AdaptToDomain(transactionRequest)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	transactionResponse, err := json.Marshal(adapters.AdaptToResponse(transaction))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	err = s.TransactionUseCase.Create(transaction)
	if err != nil {
		http.Error(w, "Error when trying to create a transaction", http.StatusBadRequest)
		return
	}

	w.Write(transactionResponse)
}
