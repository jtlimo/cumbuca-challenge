package adapters

import (
	transaction "cumbuca/internal/transaction/domain"
)

func AdaptToDomain(transactionRequestBody TransactionRequestBody) (*transaction.Transaction, error) {
	return transaction.New(transaction.TransactionDTO(transactionRequestBody))
}

func AdaptToResponse(transaction *transaction.Transaction) TransactionResponse {
	return TransactionResponse{
		Id:              transaction.TransactionId,
		AccountSender:   transaction.AccountSender,
		AccountReceiver: transaction.AccountReceiver,
		Value:           transaction.Value,
		CreatedAt:       transaction.CreatedAt,
	}
}
