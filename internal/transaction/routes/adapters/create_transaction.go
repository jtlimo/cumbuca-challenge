package adapters

import (
	"time"

	"github.com/google/uuid"
)

type TransactionRequestBody struct {
	AccountSender   uuid.UUID `json:"accountSender"`
	AccountReceiver uuid.UUID `json:"accountReceiver"`
	Value           int       `json:"value"`
}

type TransactionResponse struct {
	Id              uuid.UUID `json:"id"`
	AccountSender   uuid.UUID `json:"accountSender"`
	AccountReceiver uuid.UUID `json:"accountReceiver"`
	Value           int       `json:"value"`
	CreatedAt       time.Time `json:"createdAt"`
}
