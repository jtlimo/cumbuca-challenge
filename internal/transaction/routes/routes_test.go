package routes

import (
	"bytes"
	account_domain "cumbuca/internal/account/domain"
	account "cumbuca/internal/account/repository"
	account_adapter "cumbuca/internal/account/routes/adapters"
	"cumbuca/internal/transaction/application"
	"cumbuca/internal/transaction/repository"
	"cumbuca/internal/transaction/routes/adapters"

	"cumbuca/pkg/routes"
	setup "cumbuca/tests"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
)

var testDB *sqlx.DB
var repo *repository.TransactionRepository
var accountRepository *account.AccountRepository
var s Server
var accountSender account_domain.Account
var accountReceiver account_domain.Account

func TestMain(m *testing.M) {
	testDB = setup.InitTempDB()
	defer testDB.Close()
	repo, _ = repository.New(testDB)
	accountRepository, _ = account.New(testDB)

	s = Server{
		Router:             mux.NewRouter(),
		TransactionUseCase: application.NewTransactionUC(accountRepository, repo),
		Middleware:         routes.New(accountRepository),
	}

	accountSender = account_domain.Account{
		AccountId: setup.AccountSenderUUID,
		AccountDTO: account_domain.AccountDTO{
			Name:     "Carlos",
			LastName: "Fernandes",
			Balance:  200,
			CPF:      "77315258051",
		},
		CreatedAt: setup.Now,
	}

	accountReceiver = account_domain.Account{
		AccountId: setup.AccountReceiverUUID,
		AccountDTO: account_domain.AccountDTO{
			Name:     "Tayane",
			LastName: "Maniero",
			Balance:  100,
			CPF:      "61600681050",
		},
		CreatedAt: setup.Now,
	}

	accountRepository.CreateAccount(&accountSender)
	accountRepository.CreateAccount(&accountReceiver)

	code := m.Run()
	os.Exit(code)
}

func TestCreateTransaction(t *testing.T) {
	newServer(t)
	t.Run("create a transaction", func(t *testing.T) {
		authResponse, _ := authenticate(account_adapter.AuthenticateRequestBody{AccountId: accountSender.AccountId.String()})

		authBody, _ := io.ReadAll(authResponse.Body)

		var jsonAuthData account_adapter.AuthenticateResponse
		json.Unmarshal(authBody, &jsonAuthData)

		transaction := adapters.TransactionResponse{
			AccountSender:   setup.AccountSenderUUID,
			AccountReceiver: setup.AccountReceiverUUID,
			Value:           100,
		}

		payload := adapters.TransactionRequestBody{
			AccountSender:   setup.AccountSenderUUID,
			AccountReceiver: setup.AccountReceiverUUID,
			Value:           100,
		}

		fmt.Sprintln("Token", jsonAuthData.Token)

		request := createTransaction(payload, jsonAuthData.Token)
		res := executeRequest(request)

		body, _ := io.ReadAll(res.Body)

		var jsonData adapters.TransactionResponse
		json.Unmarshal(body, &jsonData)

		if assert.NotNil(t, jsonData) {
			assert.Equal(t, transaction.AccountReceiver, jsonData.AccountReceiver)
			assert.Equal(t, transaction.AccountSender, jsonData.AccountSender)
			assert.Equal(t, transaction.Value, jsonData.Value)
		}
		assertStatus(t, res.Code, http.StatusOK)
	})
}

func newServer(t *testing.T) {
	t.Helper()
	s.RegisterWithAuthMW()
	localServer := httptest.NewServer(s.Router)
	defer localServer.Close()
}

func createTransaction(payload adapters.TransactionRequestBody, jwtToken string) *http.Request {
	body, _ := json.Marshal(payload)
	req, _ := http.NewRequest(http.MethodPost, "/transaction", bytes.NewBuffer(body))

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", jwtToken))

	return req
}

func authenticate(payload account_adapter.AuthenticateRequestBody) (*http.Response, error) {
	body, _ := json.Marshal(payload)
	req, err := http.Post("http://localhost:3000/auth", "application/json", bytes.NewBuffer(body))

	if err != nil {
		return nil, err
	}

	return req, nil
}

func assertStatus(t *testing.T, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("did not get correct status, got %d, want %d", got, want)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	s.Router.ServeHTTP(rr, req)

	return rr
}
