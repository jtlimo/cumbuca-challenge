package tests

import (
	"log"
	"time"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/google/uuid"
	"github.com/huandu/go-sqlbuilder"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

func InitTempDB() *sqlx.DB {
	db, err := sqlx.Connect("sqlite3", ":memory:")
	if err != nil {
		log.Fatalln("failed to connect", err)
	}

	for _, schema := range getSchemas() {
		_, err := db.Exec(schema)
		if err != nil {
			log.Fatalln("failed to create table", err)
		}
	}
	return db
}

func getSchemas() []string {
	accountsTableSql, _ := sqlbuilder.NewCreateTableBuilder().
		CreateTable("accounts").
		IfNotExists().
		Define("id", "integer", "CONSTRAINT", "accounts_pk", "PRIMARY KEY", "AUTOINCREMENT", "NOT NULL").
		Define("account_id", "text", "NOT NULL").
		Define("name", "varchar", "NOT NULL").
		Define("last_name", "varchar", "NOT NULL").
		Define("cpf", "varchar", "NOT NULL").
		Define("balance", "integer", "NOT NULL").
		Define("created_at", "timestamp", "NOT NULL DEFAULT CURRENT_TIMESTAMP").
		Define("updated_at", "timestamp", "NOT NULL DEFAULT CURRENT_TIMESTAMP").
		Build()

	transactionsTableSql, _ := sqlbuilder.NewCreateTableBuilder().
		CreateTable("transactions").
		IfNotExists().
		Define("id", "integer", "CONSTRAINT", "transactions_pk", "PRIMARY KEY", "AUTOINCREMENT", "NOT NULL").
		Define("transaction_id", "text", "NOT NULL").
		Define("account_receiver", "text", "NOT NULL").
		Define("account_sender", "text", "NOT NULL").
		Define("value", "int", "NOT NULL").
		Define("created_at", "timestamp", "NOT NULL DEFAULT CURRENT_TIMESTAMP").
		Build()

	return []string{accountsTableSql, transactionsTableSql}
}

var (
	Now                           = time.Now()
	AccountUUID                   = uuid.MustParse("e8276e31-9a87-4cf1-a16c-080f9c5790d1")
	AuthUUID                      = uuid.MustParse("70c22af6-3339-4e82-afd1-30deab7bb7cf")
	AccountReceiverUUID           = uuid.MustParse("b3df1ce6-6a63-4ae1-884a-6792f4063ab5")
	AccountReceiverInexistentUUID = uuid.MustParse("54edf22b-966e-46fd-9d51-0b5ae9fd5f13")
	AccountSenderUUID             = uuid.MustParse("e81ca93b-d022-4e94-a936-585ef20767ab")
	TransactionUUID               = uuid.MustParse("ae88cd18-a17c-4b7a-9ad9-960dd89d4a68")
	TransactionNotFoundUUID       = uuid.MustParse("fcfd353f-c9e9-47d4-b468-cc52bd9fe3b5")
)
