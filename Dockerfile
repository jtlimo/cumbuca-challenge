FROM golang:1.21.3-alpine3.18 as build
WORKDIR /app
COPY go.mod go.sum ./
COPY . .
RUN go build -o bin/cumbuca cmd/server/main.go

FROM scratch as binary
COPY --from=build /app/bin/cumbuca .
EXPOSE 3000
CMD [ "./cumbuca" ]