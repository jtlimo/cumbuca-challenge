CUMBUCA_CONFIG_PATH ?= ./config/migration.config.yml

run:
	go run cmd/server/main.go

up:
	make migrations-up
	docker compose up -d

build:
	go build -o bin/cumbuca cmd/server/main.go

test:
	go test ./...

down:
	docker compose down

kill:
	docker compose kill

start:
	make down
	make up

build-image:
	docker compose build

start-database-container:
	docker compose --env-file $(CUMBUCA_CONFIG_PATH) up postgres -d 

start-application-container:
	docker compose up api -d

down-database-container:
	docker compose down postgres

down-application-container:
	docker compose down server

migrations-up:
	make start-database-container
	go run cmd/migration/main.go up

migrations-down:
	make down-database-container
	make start-database-container
	go run cmd/migration/main.go down

migrations-create:
	go run cmd/migration/main.go create --name $(name)
