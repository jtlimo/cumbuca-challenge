BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE transactions
(
    id                  bigserial       CONSTRAINT transactions_pk PRIMARY KEY,
    transaction_id      uuid            NOT NULL,
    account_receiver    uuid            NOT NULL,
    account_sender      uuid            NOT NULL,
    value               int             NOT NULL,
    created_at          timestamp       DEFAULT now() NOT NULL
);

CREATE UNIQUE INDEX transactions_id_uindex
    ON transactions (id);
COMMIT;
