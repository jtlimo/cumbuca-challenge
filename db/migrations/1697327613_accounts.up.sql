BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE accounts
(
    id              bigserial       CONSTRAINT accounts_pk PRIMARY KEY,
    account_id      uuid            NOT NULL,
    name            varchar         NOT NULL,
    last_name       varchar         NOT NULL,
    cpf             varchar         NOT NULL,
    balance         int             NOT NULL,
    created_at      timestamp       DEFAULT now() NOT NULL,
    updated_at      timestamp       DEFAULT now() NOT NULL
);

CREATE UNIQUE INDEX accounts_id_uindex
    ON accounts (id);
COMMIT;
