## Linguagem de programação

>Escolhi Go como linguagem de programação pois tenho maior familiaridade além de ser simples e performática. 
Go ajuda bastante a construir sistemas robustos e performáticos sem a adição de muitos pacotes externos.

## Secret Token como string no código

>Utilizei essa abordagem de token por ser a mais simples para implementar rapidamente a autenticação, dado o tempo disponibilizado para o teste.
Meu desejo era progredir para formas mais seguras, mas não houve tempo hábil para tal.

## Banco de dados em memória

>Utilizei o banco de dados em memória para implementar testes de integração mais robustos e rápidos. Com a possibilidade de não haver interferência entre os dados salvos durante um teste e outro. 

## Middleware de autenticação

>Utilizei esta abordagem por facilitar o encapsulamento dos handlers das rotas, além de separar bem os routers que serão protegidos dos que não serão.

## SQL puro ao invés de ORM

>Em Go é comum não se usar ORM's e na minha experiência em Node.js com Sequelize o ORM não ajuda tanto quanto se espera. Fazendo difícil a atualização da ferramenta, além de geralmente gerar queries não tão performáticas.

## Migration

>Utilizei migrations para facilitar a questão do deploy e na hora de subir o ambiente de desenvolvimento.
