package main

import (
	"context"
	account "cumbuca/internal/account/application"
	account_repository "cumbuca/internal/account/repository"
	account_routes "cumbuca/internal/account/routes"
	transaction "cumbuca/internal/transaction/application"
	transaction_repository "cumbuca/internal/transaction/repository"
	transaction_routes "cumbuca/internal/transaction/routes"
	"cumbuca/pkg/config"
	database "cumbuca/pkg/database/postgres"
	applogger "cumbuca/pkg/logger"
	middleware "cumbuca/pkg/routes"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/log"
	"golang.org/x/sync/errgroup"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var (
	logger     log.Logger
	httpServer *http.Server
)

func main() {
	logger = applogger.NewLogger()
	logger.Log("msg", "service started")

	cfg, err := loadConfig()
	if err != nil {
		logger.Log("exit", err)
		os.Exit(-1)
	}

	db, err := database.Connect(cfg.DB)
	if err != nil {
		logger.Log("msg", "database error", err)
		os.Exit(1)
	}

	accountRepository, err := account_repository.New(db)
	transactionRepository, err := transaction_repository.New(db)

	if err != nil {
		logger.Log("msg", "unable to start repository", err)
		os.Exit(1)
	}

	authenticationMiddleware := middleware.New(accountRepository)
	accountUseCase := account.NewAccountUC(accountRepository)
	transactionUseCase := transaction.NewTransactionUC(accountRepository, transactionRepository)

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(interrupt)

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		router := mux.NewRouter()
		accSrv := account_routes.Server{
			Router:         router,
			AccountUseCase: accountUseCase,
			Config:         cfg,
			Middleware:     authenticationMiddleware,
		}

		transactionSrv := transaction_routes.Server{
			Router:             router,
			TransactionUseCase: transactionUseCase,
			Config:             cfg,
			Middleware:         authenticationMiddleware,
		}

		accSrv.RegisterWithoutAuthMW()
		accSrv.RegisterWithAuthMW()

		transactionSrv.RegisterWithAuthMW()

		if err := http.ListenAndServe(":3000", router); err != http.ErrServerClosed {
			return err
		}
		return nil
	})

	select {
	case <-interrupt:
		break
	case <-ctx.Done():
		break
	}

	logger.Log("msg", "received shutdown signal")

	shutdownCtx, shutdownCancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdownCancel()

	if httpServer != nil {
		if err := httpServer.Shutdown(shutdownCtx); err != nil {
			logger.Log("msg", "server wasn't gracefully shutdown")
			defer os.Exit(2)
		}
	}

	if err := g.Wait(); err != nil {
		logger.Log("msg", "server returning an error", "error", err) //nolint: errcheck
		defer os.Exit(2)
	}

	logger.Log("msg", "service ended")
}

func loadConfig() (*config.Config, error) {
	configPath := os.Getenv("CUMBUCA_CONFIG_PATH")
	if configPath == "" {
		configPath = "./config.yml"
	}

	cfg, err := config.NewConfig(configPath)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
