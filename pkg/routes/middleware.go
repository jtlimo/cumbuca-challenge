package routes

import (
	"cumbuca/internal/account/repository"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

type Middleware struct {
	accountRepository *repository.AccountRepository
}

func New(repository *repository.AccountRepository) *Middleware {
	return &Middleware{accountRepository: repository}
}

func (m *Middleware) Authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")

		if len(tokenString) == 0 {
			http.Error(w, "Missing Authorization header", http.StatusUnauthorized)
			return
		}

		authHeader := strings.Split(tokenString, " ")
		if len(authHeader) != 2 || strings.ToLower(authHeader[0]) != "bearer" {
			http.Error(w, "Invalid Authorization header format", http.StatusUnauthorized)
			return
		}

		parsedToken := authHeader[1]

		token, err := DecodeToken(parsedToken)
		if err != nil {
			http.Error(w, "Error verifying JWT token", http.StatusUnauthorized)
			return
		}

		if err := m.verifyAccountExists(token, w, r); err != nil {
			http.Error(w, "Error verifying account existence", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) verifyAccountExists(token *jwt.Token, w http.ResponseWriter, r *http.Request) error {
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		accountId, ok := claims["account_id"].(string)
		if !ok {
			http.Error(w, "Error when getting account id", http.StatusUnauthorized)
		}
		_, err := m.accountRepository.GetById(uuid.MustParse(accountId))

		if err != nil {
			http.Error(w, "Error when getting account from database", http.StatusUnauthorized)
		}

		r.Header.Set("AccountId", accountId)
	}
	return nil
}
