package routes

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
)

func CreateToken(accountId string) (string, error) {
	tokenSecret := []byte("secretToken")
	expirationTime := time.Now().Add(1 * time.Minute)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{"account_id": accountId, "exp": expirationTime.Unix()})
	return token.SignedString(tokenSecret)
}

func DecodeToken(token string) (*jwt.Token, error) {
	tokenSecret := []byte("secretToken")
	return jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return tokenSecret, nil
	})
}
