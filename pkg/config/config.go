package config

import (
	"errors"
	"os"

	"github.com/sherifabdlnaby/configuro"
)

type Config struct {
	DB     *DBConfig     `validate:"required"`
	Server *ServerConfig `validate:"required"`
	Secret *SecretConfig `validate:"required"`
}

type DBConfig struct {
	Connection string `validate:"required"`
	Host       string `validate:"required"`
	Port       string `validate:"required"`
	Username   string `validate:"required"`
	Password   string `validate:"required"`
	Name       string `validate:"required"`
}

type ServerConfig struct {
	Host string `validate:"required"`
}

type SecretConfig struct {
	Jwt string
}

func NewConfig(configPath string) (*Config, error) {
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		return nil, errors.New("Config file does not exists")
	}

	loader, err := configuro.NewConfig(
		configuro.WithLoadFromConfigFile(configPath, false),
		configuro.WithLoadFromEnvVars("CUMBUCA"))

	if err != nil {
		return nil, err
	}

	config := &Config{}
	if err := loader.Load(config); err != nil {
		return nil, err
	}

	if err := loader.Validate(config); err != nil {
		return nil, err
	}

	return config, nil
}
