package postgres

import (
	"cumbuca/pkg/config"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
)

func Connect(cfg *config.DBConfig) (*sqlx.DB, error) {
	dsn := fmt.Sprintf(
		"host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.Name, cfg.Username, cfg.Password)

	db, err := sqlx.Connect("postgres", dsn)
	if err != nil {
		return nil, errors.New("failed to connect to the database")
	}

	fmt.Println("Successfully connected!")

	return db, nil
}
