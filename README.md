# Cumbuca challenge
API to provide the functionalities to create an account and transactions

## Table of Contents

- [Getting started](#getting-started)
- [Prepare](#prepare)
- [Building](#building)
- [Running](#running)
- [Configuring](#configuring)
- [Testing](#testing)
- [Usage](#usage)

## Getting started
### Dependencies
- Go >= 1.21
- PostgreSQL >= 9.5 (with `uuid-ossp` extension)

---
## Prepare
Clone the repository

```bash
$ git clone git@gitlab.com:jtlimo/cumbuca-challenge.git
```

Access the project folder, and download the Go dependencies

```bash
$ go get ./...
```

It may take a while to download all the dependencies, then you are [ready to build](#building).

## Building

There are two ways that you can use to build this project. The first one will build it using your own machine,
while the second one will build it using a container runtime.

Here are the following instructions for each available option:

### Local build

It should be pretty simple, once all the dependencies are downloaded just run the following command:

```bash
$ make build
```

It will generate an executable file at the `/bin` directory inside the project folder, and probably you want to know how to [run it](#running).

### Container build

At this point, I'll assume that you have installed and configure the container runtime in your system.

```bash
$ make build-image
```

If everything works, you can now [run the service using the container image](#running).

### Local database

You can have your local database running the following command:

```bash
$ make start-database-container
```

And then you could run the migrations using

```bash
$ make migrations-up
```

_The command migrations-up will start the database before and then run the migrations_

## Running

OK! Now you build it you need to run the project. That should also be pretty easy.

### Local run

If you want to run locally, you may need to have a **Postgres** instance running and accessible
from your machine, and you may need to first [configure it](#configuring). Then you can run it, you just need to
execute the following command:

```bash
$ make run
```

Once it is running you can test it: http://localhost:3000

### Container run

If you want to run using a container runtime, the easiest way to do it is using the `docker compose`.

All that you need to do is, execute the command:

```bash
$ make start
```

It should create 2 containers, one that runs the service and another that runs the **Postgres**.

_The command make start before all down the running containers, after that runs the migrations before starting the containers._

If you already have your own **Postgres** instance you can only run the application container:

```bash
$ make start-application-container
```

Keep in mind that, in both cases, it will load the `config/config.yml` file from the project. If you want to change some
configurations you can set the environment variables in your `docker-compose.yml` file, or edit the configuration file.

Once you have the IP address you can now access the endpoint: http://localhost:3000


### Configuring

You can easily configure the application editing the `config/config.yml` file or using environment variables. If you want to use the variables, be sure to prefix it all with `CUMBUCA_`.

The list of the environment variables and it's default values:

```
CUMBUCA_DB_DRIVER = "postgres"
CUMBUCA_DB_HOST = "localhost"
CUMBUCA_DB_PORT = 5432
CUMBUCA_DB_USER = "cumbuca"
CUMBUCA_DB_PASSWORD = "cumbuca_password"
CUMBUCA_DB_NAME = "cumbuca_database"
```

    We are using configuro to manage the configuration, so the precedence order to configuration is: Environment variables > .env > Config File > Value set in Struct before loading.

## Testing

You can run all the tests with one single command:

```bash
$ make test
```

## Usage

> `POST /account`

_Create a new account. e.g:_

```bash
$ curl -X POST localhost:3000/account \
  -H 'Content-Type: application/json' \
  -d '{"name": "Leigh", "lastName": "Anne", "cpf": "00911376089", "balance": 1000}'
 ```

```json 
 {
    "id": "a960c2ce-f2dc-4935-859e-d1d4e380735a",
    "name": Leigh,
    "lastName": Anne,
    "cpf": "00911376089",
    "balance": 1000,
    "createdAt": "2023-10-13T23:02:35.288453-03:00"
 }
```


>`POST /auth`

_Authenticate an user with an existent account. e.g:_

```bash
$ curl -X POST localhost:3000/auth \
  -H 'Content-Type: application/json' \
  -d '{"accountId": "b3584d6d-3446-482f-a5b4-2a9c84174a40"}'
 ```

```json 
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiYjM1ODRkNmQtMzQ0Ni00ODJmLWE1YjQtMmE5Yzg0MTc0YTQwIiwiZXhwIjoxNjk4MTk3OTUzfQ.e8oBzBhjpR5ek6E1yQyE9ZRGeJUS0GqcV8w4SFYEimQ"
}
```


> `GET /account/{accountId}/balance`

_Get the balance from an specific account. e.g:_

```bash
$ curl -X GET localhost:3000/account/b3584d6d-3446-482f-a5b4-2a9c84174a40/balance \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiYjM1ODRkNmQtMzQ0Ni00ODJm-LWE1YjQtMmE5Yzg0MTc0YTQwIiwiZXhwIjoxNjk4MTk4MzQ2fQ.ny9UQYV9YyAz-lDmADSdKrAGqmEaFDqTph1dHy94WxE'
```

```json
{
	"balance": 1000
}  
```

> `POST /transaction`

_Make a transaction between two valid accounts with sufficient balance. e.g:_

```bash
$ curl -X POST localhost:3000/transaction \
  -H 'Content-Type: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiYjM1ODRkNmQtMzQ0Ni00ODJm-LWE1YjQtMmE5Yzg0MTc0YTQwIiwiZXhwIjoxNjk4MTk4MzQ2fQ.ny9UQYV9YyAz-lDmADSdKrAGqmEaFDqTph1dHy94WxE' \
  -d '{"accountSender": "5e3dc70a-c95e-4580-90b5-0b74e9c88993", "accountReceiver": "f0569717-7b76-43a2-8dca-f4b5b6b12e2c", "value": 1000}'
```

```json
{
	"id": "027ec1a5-22a8-4ebc-b75b-6a3d05f33409",
	"accountSender": "5e3dc70a-c95e-4580-90b5-0b74e9c88993",
  	"accountReceiver": "f0569717-7b76-43a2-8dca-f4b5b6b12e2c",
  	"value": 1000,
	"createdAt": "2023-10-25T19:59:24.207969-03:00"
}
```